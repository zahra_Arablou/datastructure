package quiz1rollingfifo;

class FIFOFullException extends Exception {

    public FIFOFullException() {
    }
}

public class RollingPriorityFIFO {

    private class Item {
        // add constructor of your choice

        Item next;
        boolean priority = false;
        String value;
    }
    private Item tail;
    private Item head;
    int itemTotal;
    int itemCurrUsed = 0;

    public RollingPriorityFIFO(int itemTotal) {
         if (itemTotal < 5) {
            throw new IllegalArgumentException("Size must be 5 or greater");
        }
        this.itemTotal = itemTotal;
        Item prevItem = null;
        for (int i = 0; i < itemTotal; i++) {
            Item item = new Item();
           // item.debugId = i;
            if (i == 0) { // first item
                head = item;
                tail = item;
            }
            if (prevItem != null) { // all but first item
                prevItem.next = item;
            }
            if (i == itemTotal - 1) { // last item
                item.next = head;
            }
            prevItem = item;
        }
        System.out.println("Items allocation complete");
//        if (itemTotal < 5) {
//            throw new IllegalArgumentException();
//        }
//        Item item1 = new Item();
//        Item item2 = new Item();
//        Item item3 = new Item();
//        Item item4 = new Item();
//        Item item5 = new Item();
//
//        item1.next = item2;
//        item2.next = item3;
//        item3.next = item4;
//        item4.next = item5;
//        item5.next = item1;
//        head = item1;
//        tail = item1;
//        this.itemTotal = itemTotal;

    }

    public void enqueue(String value, boolean priority)throws FIFOFullException{
        if (itemCurrUsed == itemTotal) {
            throw new FIFOFullException();  
        }
        if (tail.value == null) {
            tail.value = value;
            tail.priority = priority;
            itemCurrUsed++;
            return;
        }
        tail = tail.next;
        tail.value = value;
        tail.priority = priority;
        itemCurrUsed++;

    }
    
private Item getPreviousOf(Item item) {
        Item current = tail;
        for (int i = 0; i < itemTotal; i++) {
            if (current.next == item) {
                return current;
            }
            current = current.next;
        }
        return null; // should never happen
    }

    /* returns null if fifo is empty, if it is not emtpy then
    * priority=true items are sarched first
    * if none is found then non-priority item is returned
     */
    public String dequeue1() {
        if (itemCurrUsed == 0) {
            return null;
        }
        // tail always points to item to be dequeued
        if (itemCurrUsed == 1) {
            String value = tail.value;
            itemCurrUsed--;
            tail.value = null; // clean up, avoid memory leaks
            return value;
        } else {
            // find a priority item first
            Item previous = null;
            for (Item current = tail; current != head.next; current = current.next) {
                if (current.priority) { // found a priority item
                    String value = current.value;
                    current.value = null; // clean up the item
                    
                    if (current == head) { // removal at head
                        head = getPreviousOf(head);
                    } else {
                        if (previous == null) { // removing at tail pointer
                            tail = current.next;
                            previous = getPreviousOf(current);
                        }
                        previous.next = current.next; // dangerous
                        //
                        current.next = head.next;
                        head.next = current;
                        //
                    }
                    itemCurrUsed--;
                    return value;
                }
                previous = current;
            }
            // otherwise return non-priority item
            String value = tail.value;
            itemCurrUsed--;
            tail.value = null; // clean up, avoid memory leaks
            tail = tail.next; // move on to next item to be fetched
            return value;
        }
    }

   
    public String dequeue() {
        if (itemCurrUsed == 0) {
            return null;
        }

        if (head.priority == true) {//the first item has periority
            String str = head.value;
            head = head.next;
            itemCurrUsed--;
            return str;

        }
        Item prev = null;  //one of the middle items has periority
        Item current = head;
        for (int i = 0; i < itemCurrUsed; i++) {
            if (current.priority == true) {
                String str = current.value;
                prev.next = current.next;
                current.next = tail.next;
                tail.next = current;
                itemCurrUsed--;
                return str;
            }
            prev = current;
            current = current.next;
        }
        String str = head.value;//we dont have periority
        head = head.next;
        itemCurrUsed--;
        return str;

    }

    public int size() {
        return itemCurrUsed;
    } // current FIFO size

    public int sizeMax() {
        return itemTotal;
    } // maximum FIFO size

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        Item current = head;
        result.append("[");
        for (int i = 0; i < itemCurrUsed; i++) {
            result.append(i == 0 ? "" : ",");
            if (current.priority == true) {
                result.append(current.value + "*");
            } else {
                result.append(current.value);
            }
            current = current.next;
        }
        result.append("]");
        return result.toString();
    }

    public String[] toArray() {
        String[] result = new String[itemCurrUsed];
        Item current = head;
        int j = 0;
        for (int i = 0; i < itemCurrUsed; i++) {
            result[i] = current.value;

            current = current.next;
        }
//          for (int i = 0; i < result.length; i++) {
//              System.out.println(result[i]);
//              
//          }
        return result;
    }

    public String[] toArrayOnlyPriority() {
        Item current = head;
        int j = 0;
        for (int i = 0; i < itemCurrUsed; i++) {
            if (current.priority == true) {
                j++;
            }
            current = current.next;
        }
        String[] result = new String[j];
        current = head;
        j = 0;
        for (int i = 0; i < itemCurrUsed; i++) {
            if (current.priority == true) {
                result[j] = current.value + "*";
                j++;
            }
            current = current.next;
        }
        return result;
    }
}
